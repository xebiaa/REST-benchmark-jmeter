Usage
-----

 - Open master.jmx in jmeter
 - Edit the "User Defined Variables", if you do not use a context path, leave the variable empty. If you do use a context path, make sure you add a slash at the end of your context.
 - View the Aggregate Report, clear if needed and run. Keep your eye on your throughput and error rate.

The id's used for both posting and getting are:

     100001 above 8000000
    1000000 below 8000000 (should be available)

Because both the getting an posting threads will run at the same time, your error rate should be 10% or lower (if the POST is before the GET, the GET will succeed).


